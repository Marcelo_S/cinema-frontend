import axios from "axios";

export const fetchData = async () => {
  try {
    const { data: movies } = await axios.get(
      `${process.env.REACT_APP_API_URL}/movies`
    );
    const { data: actors } = await axios.get(
      `${process.env.REACT_APP_API_URL}/actors`
    );
    const { data: categories } = await axios.get(
      `${process.env.REACT_APP_API_URL}/categories`
    );

    const data = movies.map((movie) => {
      return {
        id: movie.id,
        name: movie.name,
        actor: actors.find((item) => item.id === movie.idActor).name,
        category: categories.find((item) => item.id === movie.idCategory)
          .description,
      };
    });

    return { data };
  } catch (error) {
    return {
      error,
    };
  }
};
