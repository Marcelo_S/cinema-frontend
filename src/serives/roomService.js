import axios from "axios";

export const fetchData = async () => {
  try {
    const { data } = await axios.get(`${process.env.REACT_APP_API_URL}/rooms`);
    return { data };
  } catch (error) {
    return {
      error,
    };
  }
};
