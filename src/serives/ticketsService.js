import axios from "axios";

export const fetchData = async () => {
  try {
    const { data: tickets } = await axios.get(
      `${process.env.REACT_APP_API_URL}/tickets`
    );
    const { data: movies } = await axios.get(
      `${process.env.REACT_APP_API_URL}/movies`
    );
    const { data: seats } = await axios.get(
      `${process.env.REACT_APP_API_URL}/seats`
    );
    const { data: rooms } = await axios.get(
      `${process.env.REACT_APP_API_URL}/rooms`
    );

    const data = tickets.map((ticket) => {
      return {
        id: ticket.id,
        decription: ticket.decription,
        idMovie: movies.find((movie) => movie.id === ticket.idMovie),
        idSeat: seats.find((seat) => seat.id === ticket.idSeat),
        idRoom: rooms.find((room) => room.id === ticket.idRoom),
        movie: movies.find((movie) => movie.id === ticket.idMovie).name,
        seat: seats.find((seat) => seat.id === ticket.idSeat).number,
        room: rooms.find((room) => room.id === ticket.idRoom).number,
      };
    });

    const seatActive = seats.filter((seat) => seat.state !== 0);

    return {
      data,
      movies: movies.map((mov) => ({ label: mov.name, value: mov.id })),
      seats: seatActive.map((seat) => ({ label: seat.number, value: seat.id })),
      rooms: rooms.map((room) => ({ label: room.number, value: room.id })),
    };
  } catch (error) {
    return {
      error,
    };
  }
};

export const create = async (ticket) => {
  try {
    await axios.post(`${process.env.REACT_APP_API_URL}/tickets`, ticket);

    // disable seat
    const { data } = await axios.get(`${process.env.REACT_APP_API_URL}/seats`);

    const seat = data.find((seat) => seat.id === ticket.IdSeat);

    await axios.put(`${process.env.REACT_APP_API_URL}/seats/${ticket.IdSeat}`, {
      id: seat.id,
      number: seat.number,
      status: false,
    });
  } catch (error) {
    console.log("error", error);
  }
};
