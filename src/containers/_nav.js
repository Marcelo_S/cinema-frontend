export default [
  {
    _tag: "CSidebarNavTitle",
    _children: ["Management"],
  },
  {
    _tag: "CSidebarNavItem",
    name: "Actor",
    to: "/actors",
    icon: "cil-drop",
  },
  {
    _tag: "CSidebarNavItem",
    name: "Tickets",
    to: "/tickets",
    icon: "cil-drop",
  },
  {
    _tag: "CSidebarNavItem",
    name: "Movies",
    to: "/movies",
    icon: "cil-drop",
  },
  {
    _tag: "CSidebarNavItem",
    name: "Rooms",
    to: "/rooms",
    icon: "cil-drop",
  },
];
