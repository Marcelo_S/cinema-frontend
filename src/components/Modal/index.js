import React from "react";
import propTypes from "prop-types";
import { CModal, CModalBody, CModalHeader } from "@coreui/react";

const Modal = (props) => {
  const { show, toggleModal, title, children } = props;
  return (
    <div className="container">
      <div className="row">
        <CModal show={show} onClose={toggleModal}>
          <CModalHeader closeButton>{title}</CModalHeader>
          <CModalBody>{children}</CModalBody>
        </CModal>
      </div>
    </div>
  );
};

Modal.propTypes = {
  show: propTypes.bool,
  title: propTypes.string,
  toggleModal: propTypes.func,
};

Modal.defaultProps = {
  show: false,
  title: "test",
};

export default Modal;
