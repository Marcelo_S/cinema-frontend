import React, { Fragment } from "react";
import propTypes from "prop-types";
import {
  CBadge,
  CCard,
  CCardBody,
  CCardHeader,
  CDataTable,
} from "@coreui/react";

const Table = (props) => {
  const {
    showActions,
    fields,
    isActiveColumn,
    isUpdated,
    updateAction,
    isRemoved,
    deleteAction,
    tableName,
    isCreated,
    toggleModal,
    items,
  } = props;

  const fieldsItems = showActions
    ? [
        ...fields,
        {
          key: "actions",
          label: "Actions",
          _style: { width: "20%" },
          sorter: false,
          filter: false,
        },
      ]
    : fields;

  const getBageColor = (status) => {
    switch (status) {
      case "success":
        return "success";
      case "warning":
        return "warning";
      case "failed":
        return "danger";
      case "pendding":
        return "warning";
      default:
        return "primary";
    }
  };

  const renderBadge = (item) => {
    return (
      <td>
        <CBadge color={getBageColor(item.status)}>
          {isActiveColumn
            ? item.status === "success"
              ? "active"
              : "inactive"
            : item.status}
        </CBadge>
      </td>
    );
  };

  const renderActions = (item) => {
    return (
      <td className="py-2">
        {isUpdated ? (
          <button
            className="btn btn-warning"
            size="sm"
            onClick={updateAction(item)}
          >
            Update
          </button>
        ) : null}
        {isRemoved ? (
          <button
            className="btn bnt-danger ml-1"
            size="sm"
            onClick={deleteAction(item.id)}
          >
            Delete
          </button>
        ) : null}
      </td>
    );
  };

  return (
    <Fragment>
      <div className="container">
        <div className="row">
          <div className="col-12">
            <CCard>
              <CCardHeader>
                <div className="d-flex flex-row justify-content-between align-items-center">
                  <p>{tableName}</p>

                  {isCreated && (
                    <button onClick={toggleModal} className="btn btn-success">
                      Create
                    </button>
                  )}
                </div>
              </CCardHeader>
              <CCardBody>
                <CDataTable
                  items={items}
                  fields={fieldsItems}
                  scopedSlots={{
                    status: (item) => renderBadge(item),
                    actions: (item) => renderActions(item),
                  }}
                />
              </CCardBody>
            </CCard>
          </div>
        </div>
      </div>
    </Fragment>
  );
};

Table.propTypes = {
  fields: propTypes.array.isRequired,
  items: propTypes.array.isRequired,
  deleteAction: propTypes.func,
  updateAction: propTypes.func,
  showActions: propTypes.bool,
  isRemoved: propTypes.bool,
  isCreated: propTypes.bool,
  isUpdated: propTypes.bool,
  isActiveColumn: propTypes.bool,
  toggleModal: propTypes.func,
};

Table.defaultProps = {
  deleteAction: () => {},
  updateAction: () => {},
  showActions: false,
  isUpdated: false,
  isCreated: false,
  isActiveColumn: false,
  isRemoved: false,
};

export default Table;
