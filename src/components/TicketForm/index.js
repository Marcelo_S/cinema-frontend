import React, { useState, useRef } from "react";
import propTypes from "prop-types";
import Select from "react-select";

const Form = (props) => {
  const { toggleModal, movies, seats, rooms, createTicket } = props;

  const [ticket, setTicket] = useState({});
  const [movieSelect, setMovieSelect] = useState({});
  const [seatSelect, setSeatSelect] = useState({});
  const [roomSelect, setRoomSelect] = useState({});
  const formRef = useRef(null);

  const reset = () => {
    formRef.current.reset();
    setMovieSelect({});
    setRoomSelect({});
    setSeatSelect({});

    return {};
  };

  const handleChange = ({ target: { name, value } }) => {
    setTicket({
      ...ticket,
      [name]: value,
    });
  };

  const handleMovie = ({ value, label }) => {
    setTicket({
      ...ticket,
      IdMovie: value,
    });

    setMovieSelect({
      value,
      label,
    });
  };

  const handleSeat = ({ value, label }) => {
    setTicket({
      ...ticket,
      IdSeat: value,
    });

    setSeatSelect({
      value,
      label,
    });
  };

  const handleRoom = ({ value, label }) => {
    setTicket({
      ...ticket,
      IdRoom: value,
    });

    setRoomSelect({
      value,
      label,
    });
  };

  const handleSubmit = (event) => {
    event.preventDefault();

    createTicket(ticket);

    reset();
    toggleModal();
  };

  return (
    <form ref={formRef} onSubmit={handleSubmit}>
      <div className="form-group">
        <label htmlFor="description"> Description </label>
        <input
          className="form-control"
          onChange={handleChange}
          id="description"
          name="description"
          placeholder="Description ticket"
        />
      </div>
      <div className="form-group">
        <label htmlFor="movies"> Movies </label>
        <Select
          id="movies"
          name="movies"
          options={movies}
          value={movieSelect}
          onChange={handleMovie}
        />
      </div>
      <div className="form-group">
        <label htmlFor="application"> Seat </label>
        <Select
          id="seat"
          name="seat"
          options={seats}
          value={seatSelect}
          onChange={handleSeat}
        />
      </div>
      <div className="form-group">
        <label htmlFor="application"> Room </label>
        <Select
          id="room"
          name="room"
          options={rooms}
          value={roomSelect}
          onChange={handleRoom}
        />
      </div>
      <div className="flex-center">
        <div className="form-actions">
          <button type="submit" className="btn btn-primary">
            Save
          </button>
          <button
            type="reset"
            onClick={toggleModal}
            className="btn btn-secondary ml-3"
          >
            Cancel
          </button>
        </div>
      </div>
    </form>
  );
};

Form.propTypes = {
  toggleModal: propTypes.func.isRequired,
  createTicket: propTypes.func.isRequired,
  movies: propTypes.array,
  rooms: propTypes.array,
  seats: propTypes.array,
};

Form.defaultProps = {
  edit: false,
};

export default Form;
