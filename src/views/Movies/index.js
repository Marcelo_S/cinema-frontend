import React, { useState, useEffect } from "react";
import Table from "../../components/Table";
import { fetchData } from "../../serives/movieService";

const Movies = (props) => {
  const columns = ["id", "name", "actor", "category"];

  const [data, setData] = useState([]);

  useEffect(() => {
    fetchDataMovie();
  }, []);

  const fetchDataMovie = async () => {
    const { data } = await fetchData();
    setData(data);
  };

  return <Table items={data || []} fields={columns} tableName="Movies" />;
};

export default Movies;
