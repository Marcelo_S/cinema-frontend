import React, { useState, useEffect } from "react";
import Table from "../../components/Table";
//import actorData from '../../data/actor.json'
import { fetchData } from "../../serives/actorService";

const Actor = (props) => {
  const columns = ["id", "name", "lastname"];

  const [data, setData] = useState([]);

  useEffect(() => {
    fetchDataActor();
  }, []);

  const fetchDataActor = async () => {
    const { data } = await fetchData();
    setData(data);
  };

  return <Table items={data || []} fields={columns} tableName="Actores" />;
};

export default Actor;
