import React, { Fragment, PureComponent } from "react";
import Table from "../../components/Table";
import Form from "../../components/TicketForm";
import Modal from "../../components/Modal";
import { fetchData, create } from "../../serives/ticketsService";

class Ticket extends PureComponent {
  constructor(props) {
    super(props);
    this.columns = ["id", "decription", "movie", "room", "seat"];

    this.state = {
      tickets: [],
      movies: [],
      seats: [],
      rooms: [],
      showModal: false,
    };
  }

  componentDidMount() {
    this.fetchData();
  }

  fetchData = async () => {
    const { data, movies, seats, rooms } = await fetchData();

    this.setState({
      tickets: data,
      movies,
      seats,
      rooms,
    });
  };

  toggleModal = () => {
    this.setState((prevState) => {
      return {
        showModal: !prevState.showModal,
      };
    });
  };

  createTicket = async (ticket) => {
    await create(ticket);
    this.fetchData();
  };

  render() {
    return (
      <Fragment>
        <Table
          items={this.state.tickets || []}
          fields={this.columns}
          tableName="Tickets"
          isCreated
          toggleModal={this.toggleModal}
        />
        <Modal
          title="Create Ticket"
          toggleModal={this.toggleModal}
          show={this.state.showModal}
        >
          <Form
            toggleModal={this.toggleModal}
            createTicket={this.createTicket}
            rooms={this.state.rooms}
            seats={this.state.seats}
            movies={this.state.movies}
          />
        </Modal>
      </Fragment>
    );
  }
}

export default Ticket;
