import React, { useState, useEffect } from "react";
import Table from "../../components/Table";
import { fetchData } from "../../serives/roomService";

const Rooms = (props) => {
  const columns = ["id", "number"];

  const [data, setData] = useState([]);

  useEffect(() => {
    fetchDataRooms();
  }, []);

  const fetchDataRooms = async () => {
    const { data } = await fetchData();
    setData(data);
  };

  return <Table items={data || []} fields={columns} tableName="Rooms" />;
};

export default Rooms;
