import React from "react";
// custom components
const Actor = React.lazy(() => import("./views/Actors"));
const Ticket = React.lazy(() => import("./views/Tickets"));
const Movies = React.lazy(() => import("./views/Movies"));
const Rooms = React.lazy(() => import("./views/Rooms"));

const routes = [
  { path: "/actors", exact: true, name: "Actor", component: Actor },
  { path: "/tickets", exact: true, name: "Actor", component: Ticket },
  { path: "/movies", exact: true, name: "Movies", component: Movies },
  { path: "/rooms", exact: true, name: "Rooms", component: Rooms },
];

export default routes;
